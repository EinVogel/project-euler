"""""
Euler task 3
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
"""""
import time

# Start of time
startTime = time.time()

number = 600851475143
# start dividing the divisor from this number to determine if it is a Prime number or not
start = 2
# An empty variable. Stop shows up to what number to look for
stop = 0
# This is an array of the results of dividing the divisor by a range of numbers from start to stop
length = []
# signal to end loop 1
label = 0
# The Loop 1. Looking for the max divider for loop 2
for i in range(2, number):
    a = number % i
    if a == 0:
        maxDivider = int(number / i)
        # To maxDivider itself is not shared and the array was not the remainder of the division 0
        stop = int(maxDivider / 2)
        # The Loop 2. Determine the simple divisor or not by multiplying the array
        for element in range(start, stop):
            m = maxDivider % element
            length.append(m)
            if length.count(0) == 1:
                break
            elif length.count(0) == 0 and len(length) == stop - start:
                print('The largest prime factor of the number', number, 'is', maxDivider)
                label = 1
                break
        if label == 1:
            break
        length = []

# End of time
endTime = time.time()
# We can see the program execution time
print('The program execution time is', endTime - startTime)
