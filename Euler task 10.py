"""""
Euler task 10
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""""
import time

# Start of time
start = time.time()

divider = 3
number = 1
label = 0
m = 2
while divider <= 2000000:
    for i in range(2, int((divider ** 0.5) + 2)):
        label = 1
        if divider % i == 0:
            label = 0
            break
    if label == 1:
        number += 1
        m = m + divider
    divider += 2
print('the sum of all the primes below two million is', m)

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
