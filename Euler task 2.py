"""""
Euler task 2
Each new term in the Fibonacci sequence is generated by adding the previous two terms.
By starting with 1 and 2, the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence whose values do not exceed four million.
Find the sum of the even-valued terms.
"""""
import time

# Start of time
start = time.time()

# Take first two terms of Fibonacci sequence, and create the list
arrOfFibonacci = [1, 2]
numberOfElement = 2
nextTerm = 0
maxNumber = 4000_000
while nextTerm < maxNumber:

    nextTerm = arrOfFibonacci[numberOfElement - 1] + arrOfFibonacci[numberOfElement - 2]
    if nextTerm > maxNumber:
        break
    arrOfFibonacci.append(nextTerm)
    numberOfElement += 1

evenNumbers = [n for n in arrOfFibonacci if n % 2 == 0]
print('The sum of the Fibonacci sequence whose values do not exceed four million is', sum(evenNumbers))

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
