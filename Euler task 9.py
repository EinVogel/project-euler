"""""
Euler task 9
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""""
import time

# Start of time
start = time.time()

label = 0
for a in range(1, 1000):
    for b in range(1, 1000):
        if a + b + (a ** 2 + b ** 2) ** 0.5 == 1000:
            label = 1
            break
    if label == 1:
        break

print('a =', a, 'b =', b)
c = 1000 - a - b
print('The product abc is', a * b * c)

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)

