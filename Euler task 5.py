"""""
Euler task 5
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""""
import time

# Start of time
start = time.time()

endOfNumbers = 20
prodOfNumbers = 1
stop = endOfNumbers + 1
arr = []
listOfPrime = []
multiplier = 1
# Find a list of Prime numbers in the range from 1 to endOfNumbers
for i in range(1, stop):
    for element in range(1, stop):
        m = i % element
        arr.append(m)
    if arr.count(0) == 2:
        listOfPrime.append(i)
    arr = []

for i in listOfPrime:
    prodOfNumbers = prodOfNumbers * i

label = 0
for i in range(1, 100000000000000000):
    for element in range(1, stop):
        if prodOfNumbers * i % element == 0:
            label = 1
            continue
        else:
            label = 0
            break
    if label == 1:
        print('The smallest positive number that is evenly divisible by all of the numbers from 1 to 20',
              i * prodOfNumbers)
        multiplier = i
        break
    else:
        i += 1

divider = prodOfNumbers * multiplier

# Test
label = 0
arr = []
for i in range(1, stop):
    m = divider % i
    print(divider, ':', i, '=', m)
    arr.append(m)

if sum(arr) == 0:
    print('The test is successful')
else:
    print(sum(arr))

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
