"""""
Euler task 4
A palindromic number reads the same both ways.
 The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
"""""
import time

# Start of time
start = time.time()

maxSymbols = 999
minSymbols = 100
label = 0
for i in range(maxSymbols, minSymbols, -1):
    for element in range(maxSymbols, minSymbols, -1):
        palindrome = i * element
        palindrome = str(palindrome)
        if palindrome[0] == palindrome[-1] and palindrome[1] == palindrome[-2] and palindrome[2] == palindrome[-3]:
            print('The largest palindrome made from the product of two 3-digit numbers is', palindrome)
            label = 1
            break
    if label == 1:
        break

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
