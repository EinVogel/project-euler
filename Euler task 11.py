"""""
Euler task 11
In the 20×20 grid below, four numbers along a diagonal line have been marked in red.
The product of these numbers is 26 × 63 × 78 × 14 = 1788696.

What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally)
 in the 20×20 grid?
"""""
import numpy as np
import time

# Start of time
start = time.time()

a = open("Euler task 11.txt")
b = (a.read())
b = b.replace("\n", ' ')
arr = []
arrOfNumbers = []
for i in b:
        arr.append(i)
m = 0
k = 1
for i in range(0, len(arr), 3):
    number = arr[m] + arr[k]
    number = int(number)
    arrOfNumbers.append(number)
    m += 3
    k += 3

arrOfNumbers = np.array(arrOfNumbers)
# converting a line array to 20 x 20
arrOfNumbers.shape = (20, 20)

f = 1
listOfProd1 = []
listOfProd2 = []
listOfProd3 = []
listOfProd4 = []
for i in range(0, 17):  # 17 because the main matrix 20-4 small matrix == 17. Loop vertically.
    for element in range(0, 17):
        tempMatrix = arrOfNumbers[i:i + 4, element:element + 4]  # tempMatrix temporary array 4 х 4
        #  Loop to find the product of diagonal 1
        for cell in range(0, 4):
            f = f * tempMatrix[cell, cell]
        listOfProd1.append(f)
        f = 1
        #  Loop to find the product of diagonal 2
        for cell in range(0, 4):
            f = f * tempMatrix[3 - cell, cell]
        listOfProd2.append(f)
        f = 1
        #  Loop to find the product along a horizontal line
        for cell in range(0, 4):
            for s in range(0, 4):
                f = f * tempMatrix[cell, s]
            listOfProd3.append(f)
            f = 1
        #  Loop to find the product along a vertical line
        for cell in range(0, 4):
            for s in range(0, 4):
                f = f * tempMatrix[s, cell]
            listOfProd4.append(f)
            f = 1
arrOfAnswers = [max(listOfProd1), max(listOfProd2), max(listOfProd3), max(listOfProd4)]
print('the greatest product of four adjacent numbers in the same direction is', max(arrOfAnswers))

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
