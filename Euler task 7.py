"""""
Euler task 7
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 1001st prime number?
"""""
import time

# Start of time
start = time.time()

divider = 3
number = 1
label = 0
while number != 10001:
    label = 1
    for i in range(2, int((divider ** 0.5) + 2)):
        if divider % i == 0:
            label = 0
            break
    if label == 1:
        number += 1
    divider += 2
stop = time.time()
print('The 1001st prime number is', divider - 2)

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
