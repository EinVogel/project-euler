"""""
Euler task 8
The four adjacent digits in the 1000-digit number that have the greatest product are 9 × 9 × 8 × 9 = 5832.
Find the thirteen adjacent digits in the 1000-digit number that have the greatest product.
What is the value of this product?
"""""
import time

# Start of time
start = time.time()
# open the file
a = open("Euler task 8.txt")
# read
b = (a.read())
# remove the line break character
b = b.replace("\n", '')
arr = []
tempArr = []
maxProd = []
# The product of how many numbers?
numbers = 13
for i in b:
        arr.append(i)
m = 0
for i in range(1, int(1000 - numbers + 2)):
    for element in range(0, numbers):
        tempArr.append(int(arr[m + element]))
    prodOfNumbers = 1
    for k in tempArr:
        prodOfNumbers = prodOfNumbers * k
    maxProd.append(prodOfNumbers)
    tempArr = []
    m += 1
print('The thirteen adjacent digits in the 1000-digit number that have the greatest product is', max(maxProd))

# End of time
end = time.time()
# We can see the program execution time
print('The program execution time is', end - start)
